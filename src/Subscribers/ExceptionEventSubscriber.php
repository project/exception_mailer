<?php

namespace Drupal\exception_mailer\Subscribers;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormAjaxException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Utility\Error;
use Drupal\exception_mailer\ExceptionMailerExcludeManager;
use Drupal\exception_mailer\Utility\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Subscribe to thrown exceptions to send emails to admin users.
 */
class ExceptionEventSubscriber implements EventSubscriberInterface {

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The queue manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The exception exclude manager.
   *
   * @var \Drupal\exception_mailer\ExceptionMailerExcludeManager
   */
  protected $excludeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue service.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_manager
   *   The queue manager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\exception_mailer\ExceptionMailerExcludeManager $excludeManager
   *   The exception exclude manager.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger,
    QueueFactory $queue_factory,
    QueueWorkerManagerInterface $queue_manager,
    ConfigFactory $configFactory,
    AccountInterface $account,
    TimeInterface $time,
    StateInterface $state,
    ExceptionMailerExcludeManager $excludeManager,
  ) {
    $this->logger = $logger;
    $this->queueFactory = $queue_factory;
    $this->queueManager = $queue_manager;
    $this->configFactory = $configFactory;
    $this->currentUser = $account;
    $this->time = $time;
    $this->state = $state;
    $this->excludeManager = $excludeManager;
  }

  /**
   * Event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The exception event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function onException(ExceptionEvent $event): void {
    $request = $event->getRequest();

    $exception = $event->getThrowable();
    $queue = $this->queueFactory->get('manual_exception_email', TRUE);
    $queue_worker = $this->queueManager->createInstance('manual_exception_email');
    if (!$exception instanceof FormAjaxException && !$exception instanceof NotFoundHttpException) {
      $data['exception'] = get_class($exception);
      $data['message'] = $exception->getMessage() . "\n" . $exception->getTraceAsString();
      $data['site'] = $this->configFactory->get('system.site')->get('name');
      $data['timestamp'] = $this->time->getRequestTime();
      $data['user'] = $this->currentUser->id() . '(' . $this->currentUser->getDisplayName() . ')';
      $data['location'] = $request->getUri();
      $data['referrer'] = $request->headers->get('Referer', '');
      $data['hostname'] = $request->getClientIp();

      $config = $this->configFactory->get('exception_mailer.settings');
      if ($config->get('max_similar_emails')) {
        $email_should_be_sent = exception_mailer__last_message_similarity('exception_mailer.log.exception', $data);

        if (!$email_should_be_sent) {
          return;
        }
      }

      if ($excludes = $this->excludeManager->getExcludes($data, 'exception')) {
        /** @var \Drupal\exception_mailer\ExceptionMailerExcludeInterface  $exclude */
        foreach ($excludes as $exclude) {
          if ($exclude->getEmailBody()) {
            $data['email_body'] = $exclude->getEmailBody();
          }
          foreach ($exclude->getEmailAddresses() as $email) {
            $data['email'] = $email;
            if ($user = user_load_by_mail($email)) {
              $data['user_langcode'] = $user->getPreferredLangcode();
            }
            $data['exception_mailer_exclude_id'] = $exclude->id();
            $queue->createItem($data);
          }
        }
      }
      else {
        $config_form_email_address = $this->getConfigFormEmailAdress();
        foreach ($config_form_email_address as $admin) {
          $data['email'] = $admin;
          if ($user = user_load_by_mail($admin)) {
            $data['user_langcode'] = $user->getPreferredLangcode();
          }
          $queue->createItem($data);
        }
      }

      while ($item = $queue->claimItem()) {
        /** @var object $item */
        try {
          $queue_worker->processItem($item->data);
          $queue->deleteItem($item);
          if (isset($item->data['exception_mailer_exclude_id'])) {
            $this->state->set($item->data['exception_mailer_exclude_id'] . '_last_sent', $item->data['timestamp']);
          }
        }
        catch (SuspendQueueException $e) {
          $queue->releaseItem($item);
          break;
        }
        catch (\Exception $e) {
          Error::logException($this->logger->get('exception_mailer'), $e);
        }
      }
      $this->logger->get('exception_mailer')->debug($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::EXCEPTION][] = ['onException', 60];
    return $events;
  }

  /**
   * Returns all the email addresses set in config form.
   */
  protected function getConfigFormEmailAdress(): array {
    $config = $this->configFactory->get('exception_mailer.settings');
    $email_addresses = explode(",", trim($config->get('emails')));
    if ($roles = $config->get('roles')) {
      foreach (UserRepository::getUserEmails(array_keys($roles)) as $email) {
        $email_addresses[] = $email;
      }
    }
    // Remove duplicate emails.
    return array_unique($email_addresses);
  }

}
