<?php

namespace Drupal\exception_mailer\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ExceptionMailerExcludeForm.
 *
 * Builds the form to create Exception Mailer Exclude config entities.
 *
 * @ingroup exception_mailer
 */
class ExceptionMailerExcludeForm extends EntityForm {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ExceptionMailerExcludeForm {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('email.validator'),
      $container->get('logger.factory')->get('exception_mailer_exclude')
    );
  }

  /**
   * Constructs the ExceptionMailerExcludeForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EmailValidatorInterface $email_validator, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->emailValidator = $email_validator;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\exception_mailer\ExceptionMailerExcludeInterface $exclude */
    $exclude = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $exclude->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $exclude->id(),
      '#machine_name' => [
        'exists' => '\Drupal\exception_mailer\Entity\ExceptionMailerExclude::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$exclude->isNew(),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $exclude->getDescription(),
      '#description' => $this->t('Describe this exclude.'),
    ];

    $conditions['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => [
        'exception' => $this->t('Exception'),
        'error' => $this->t('Error'),
      ],
      '#default_value' => $exclude->getType(),
    ];
    $conditions['exception'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Exception contains'),
      '#maxlength' => 255,
      '#default_value' => $exclude->getException(),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'exception'],
        ],
      ],
    ];
    $conditions['error_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Error type'),
      '#maxlength' => 255,
      '#default_value' => $exclude->getErrorType(),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'error'],
        ],
      ],
    ];
    $conditions['error_severity'] = [
      '#type' => 'select',
      '#title' => $this->t('Error severity'),
      '#options' => RfcLogLevel::getLevels(),
      '#size' => 8,
      '#multiple' => TRUE,
      '#default_value' => $exclude->getErrorSeverity(),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'error'],
        ],
      ],
    ];
    $conditions['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message contains'),
      '#maxlength' => 255,
      '#default_value' => $exclude->getMessage(),
    ];
    $conditions['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname contains'),
      '#maxlength' => 255,
      '#default_value' => $exclude->getHostname(),
    ];
    $form['conditions'] = [
      '#type' => 'fieldset',
      '#title' => 'Conditions',
    ];
    $form['conditions'][] = $conditions;

    $form['send_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send email if conditions are met.'),
      '#default_value' => $exclude->getSendEMail(),
    ];

    $email_data['email_addresses'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email addresses'),
      '#description' => $this->t('Email will be sent for entered emails.'),
      '#default_value' => implode("\n", $exclude->getEmails()),
    ];

    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    unset($roles[RoleInterface::AUTHENTICATED_ID], $roles[RoleInterface::ANONYMOUS_ID]);
    $roles = array_map(fn(RoleInterface $role) => Html::escape($role->label()), $roles);

    $email_data['roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Roles'),
      '#description' => $this->t('Email will be sent for users assigned to select role(s).'),
      '#options' => $roles,
      '#size' => min(count($roles), 8),
      '#multiple' => TRUE,
      '#default_value' => $exclude->getRoles(),
    ];
    // @todo Add subject, token support and optional sending original report.
    $email_data['email_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email body'),
      '#description' => $this->t('The text to be send in email. On empty the default error/exception text will be sent.'),
      '#default_value' => $exclude->getEmailBody(),
    ];
    $email_data['send_interval'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Send interval in minutes'),
      '#description' => $this->t('How much minute wait after the last sending to resend this error/exception.'),
      '#maxlength' => 25,
      '#default_value' => $exclude->getSendInterval(),
    ];
    $form['email_data'] = [
      '#type' => 'fieldset',
      '#title' => 'Email data',
      '#states' => [
        'visible' => [
          ':input[name="send_email"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['email_data'][] = $email_data;

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $exclude->getStatus(),
    ];

    $form['actions'] = parent::actions($form, $form_state);
    $form['actions']['submit']['#value'] = $exclude->isNew() ? $this->t('Create') : $this->t('Update');
    $form['actions']['submit']['#button_type'] = 'primary';
    $form['actions']['delete']['#weight'] = 2;

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('entity.exception_mailer_exclude.collection'),
      '#attributes' => ['class' => ['button']],
      '#weight' => 1,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    if (!$form_state->getValue('roles')) {
      $form_state->set('roles', []);
    }

    $form_state->set('emails', []);
    if (!$form_state->isValueEmpty('email_addresses')) {
      $valid = [];
      $invalid = [];
      foreach (explode("\n", trim($form_state->getValue('email_addresses'))) as $email) {
        $email = trim($email);
        if (!empty($email)) {
          if ($this->emailValidator->isValid($email)) {
            $valid[] = $email;
          }
          else {
            $invalid[] = $email;
          }
        }
      }
      if (empty($invalid)) {
        $form_state->set('emails', $valid);
      }
      elseif (count($invalid) == 1) {
        $form_state->setErrorByName('email_addresses', $this->t('%email is not a valid email address.', ['%email' => reset($invalid)]));
      }
      else {
        $form_state->setErrorByName('email_addresses', $this->t('%emails are not valid email addresses.', ['%emails' => implode(', ', $invalid)]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\exception_mailer\ExceptionMailerExcludeInterface $exclude */
    $exclude = $this->entity;
    $exclude->setEmails($form_state->get('emails'));
    $status = $exclude->save();

    $edit_link = $exclude->toLink($this->t('Edit'), 'edit-form')->toString();
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('Exclude %label has been updated.', ['%label' => $exclude->label()]));
      $this->logger->notice('Exclude %label has been updated.', [
        '%label' => $exclude->label(),
        'link' => $edit_link,
      ]);
    }
    else {
      $this->messenger()->addStatus($this->t('Exclude %label has been added.', ['%label' => $exclude->label()]));
      $this->logger->notice('Exclude %label has been added.', [
        '%label' => $exclude->label(),
        'link' => $edit_link,
      ]);
    }

    $form_state->setRedirectUrl($exclude->toUrl('collection'));

    return $status;
  }

}
