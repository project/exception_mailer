<?php

namespace Drupal\exception_mailer\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\user\RoleInterface;
use Drupal\user\RoleStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ExceptionMailerConfigForm.
 *
 * Provides a form to configure Error & Exception mailer settings.
 */
class ExceptionMailerConfigForm extends ConfigFormBase {

  /**
   * The role storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected $roleStorage;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Constructs a \Drupal\exception_mailer\ExceptionMailerConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\user\RoleStorageInterface $role_storage
   *   The role storage.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, RoleStorageInterface $role_storage, EmailValidatorInterface $email_validator) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->roleStorage = $role_storage;
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager')->getStorage('user_role'),
      $container->get('email.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'exception_mailer.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'exception_mailer_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('exception_mailer.settings');
    $form['level_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Check the level type'),
      '#options' => RfcLogLevel::getLevels(),
      '#default_value' => $config->get('level_type'),
    ];

    $roles = $this->roleStorage->loadMultiple();
    unset($roles[RoleInterface::AUTHENTICATED_ID], $roles[RoleInterface::ANONYMOUS_ID]);
    $roles = array_map(fn(RoleInterface $role) => Html::escape($role->label()), $roles);

    $form['roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Roles'),
      '#description' => $this->t('Email will be sent for users assigned to select role(s).'),
      '#options' => $roles,
      '#size' => min(count($roles), 8),
      '#multiple' => TRUE,
      '#default_value' => $config->get('roles'),
    ];

    $form['emails'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email recipient addresses'),
      '#description' => $this->t('Email will be sent for entered emails. For more than one email separate it by comma.'),
      '#default_value' => $config->get('emails'),
    ];
    $form['limits'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email sending limits'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['limits']['max_similar_emails'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum number of suppressed consecutive similar email alerts'),
      '#description' => $this->t('Upper limit of email alerts suppressed consecutively with the same or very similar message. Leave empty for no limit.'),
      '#default_value' => $config->get('max_similar_emails'),
    ];
    $form['limits']['min_similarity'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum allowed similarity level between consecutive email alerts'),
      '#field_suffix' => '%',
      '#description' => '<p>' . $this->t('Highest similarity level above which new email alerts will not be sent anymore if "Maximum number of allowed consecutive similar email alerts" has been reached from the same type and severity and email alerts are considered "consecutive" (time period between each previous and next one is smaller than 15 minutes, then 1h, then 6h and then after 24 hours). Possible values range from 0 to 100%, where 100% stands for two identical emails.') . '</p>'
      . '<p>' . $this->t('For example setting "Maximum number of suppressed consecutive similar email alerts" to 5 and "Similarity level" to 50% would mean: you will receive one email instantly for the first error, then one after 15 minutes, then 1h, 6h, and then after 24 hours if there were at least 5 similar errors in each period and errors are similar in at least 50%.') . '</p>'
      . '<p>' . $this->t("(Note that similarity level is calculated using PHP's <a href='@similar_text_url' target='_blank'>similar_text()</a> function, with all its complexity and implications.)", ['@similar_text_url' => 'http://php.net/similar_text']) . '</p>',
      '#default_value' => $config->get('min_similarity'),
      '#min' => 0,
      '#max' => 100,
      '#size' => 5,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

    // If both the Roles and Emails fields are empty than show the error.
    if (empty($form_state->getValue('roles')) && empty($form_state->getValue('emails'))) {
      $form_state->setErrorByName('error', $this->t('Please configure at least one field Roles or Email addresses.'));
    }
    // Validate the email address field.
    if (!empty($form_state->getValue('emails'))) {
      $valid = [];
      $invalid = [];
      foreach (explode(",", trim($form_state->getValue('emails'))) as $email) {
        $email = trim($email);
        if (!empty($email)) {
          if ($this->emailValidator->isValid($email)) {
            $valid[] = $email;
          }
          else {
            $invalid[] = $email;
          }
        }
      }
      if (count($invalid) == 1) {
        $form_state->setErrorByName('emails', $this->t('%email is not a valid email address.', ['%email' => reset($invalid)]));
      }
      elseif (count($invalid) > 1) {
        $form_state->setErrorByName('emails', $this->t('%emails are not valid email addresses.', ['%emails' => implode(', ', $invalid)]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $this->config('exception_mailer.settings')
      ->set('level_type', array_filter($form_state->getValue('level_type'), function ($v) {
        return $v !== 0;
      }))
      ->set('roles', $form_state->getValue('roles'))
      ->set('emails', $form_state->getValue('emails'))
      ->set('max_similar_emails', $form_state->getValue('max_similar_emails'))
      ->set('min_similarity', $form_state->getValue('min_similarity'))
      ->save();
  }

}
