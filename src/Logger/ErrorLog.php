<?php

namespace Drupal\exception_mailer\Logger;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Utility\Error;
use Drupal\exception_mailer\ExceptionMailerExcludeManager;
use Drupal\exception_mailer\Utility\UserRepository;
use Psr\Log\LoggerInterface;

/**
 * Class ErrorLog.
 *
 * Selects errors corresponding levels specified in the config to send emails.
 */
class ErrorLog implements LoggerInterface {
  use RfcLoggerTrait;
  use DependencySerializationTrait;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * The queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The queue manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The exception exclude manager.
   *
   * @var \Drupal\exception_mailer\ExceptionMailerExcludeManager
   */
  protected $excludeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ErrorLog object.
   *
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue service.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_manager
   *   The queue manager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\exception_mailer\ExceptionMailerExcludeManager $excludeManager
   *   The exception exclude manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(
    LogMessageParserInterface $parser,
    QueueFactory $queue_factory,
    QueueWorkerManagerInterface $queue_manager,
    ConfigFactory $configFactory,
    StateInterface $state,
    ExceptionMailerExcludeManager $excludeManager,
    AccountInterface $account,
  ) {
    $this->parser = $parser;
    $this->queueFactory = $queue_factory;
    $this->queueManager = $queue_manager;
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->excludeManager = $excludeManager;
    $this->currentUser = $account;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    if (empty($context)) {
      $context = $this->getDefaultContext();
    }
    if (empty($context['timestamp'])) {
      $context['timestamp'] = time();
    }
    $config = $this->configFactory->get('exception_mailer.settings');

    $levels = $config->get('level_type');

    if (empty($levels) || !\in_array($level, array_values($levels), FALSE)) {
      return;
    }

    // Remove any backtraces since they may contain an unserializable variable.
    unset($context['backtrace']);

    // Convert PSR3-style messages to \Drupal\Component\Render\FormattableMarkup
    // style, so they can be translated too in runtime.
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);

    // If the message is a TranslatableMarkup object, render it.
    $message = $message instanceof TranslatableMarkup ? Html::decodeEntities($message->render()) : $message;

    $queue = $this->queueFactory->get('manual_exception_email', TRUE);
    /** @var \Drupal\Core\Queue\QueueWorkerInterface $queue_worker */
    $queue_worker = $this->queueManager->createInstance('manual_exception_email');

    $severity = RfcLogLevel::getLevels();
    if (\in_array($level, array_values($levels), FALSE)) {
      $data['exception'] = '';
      $data['message'] = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);
      $data['site'] = $this->configFactory->get('system.site')->get('name');
      $data['timestamp'] = $context['timestamp'];
      $data['user'] = $context['uid'] . '(' . $this->currentUser->getDisplayName() . ')';
      $data['severity'] = $severity[$level]->getUntranslatedString();
      $data['severity_level'] = $level;
      $data['type'] = mb_substr($context['channel'], 0, 64);
      $data['link'] = $context['link'];
      $data['location'] = $context['request_uri'];
      $data['referrer'] = $context['referer'];
      $data['hostname'] = mb_substr($context['ip'], 0, 128);

      if ($config->get('max_similar_emails')) {
        $email_should_be_sent = exception_mailer__last_message_similarity('exception_mailer.log.' . $data['type'] . '-' . $data['severity_level'], $data);

        if (!$email_should_be_sent) {
          return;
        }
      }

      if ($excludes = $this->excludeManager->getExcludes($data, 'error')) {
        /** @var \Drupal\exception_mailer\ExceptionMailerExcludeInterface $exclude */
        foreach ($excludes as $exclude) {
          if ($exclude->getEmailBody()) {
            $data['email_body'] = $exclude->getEmailBody();
          }
          foreach ($exclude->getEmailAddresses() as $email) {
            $data['email'] = $email;
            if ($user = user_load_by_mail($email)) {
              $data['user_langcode'] = $user->getPreferredLangcode();
            }
            $data['exception_mailer_exclude_id'] = $exclude->id();
            $queue->createItem($data);
          }
        }
      }
      else {
        $config_form_email_address = $this->getConfigFormEmailAdress();
        foreach ($config_form_email_address as $admin) {
          $data['email'] = $admin;
          if ($user = user_load_by_mail($admin)) {
            $data['user_langcode'] = $user->getPreferredLangcode();
          }
          $queue->createItem($data);
        }
      }

      while ($item = $queue->claimItem()) {
        /** @var object $item */
        try {
          $queue_worker->processItem($item->data);
          $queue->deleteItem($item);
          if (isset($item->data['exception_mailer_exclude_id'])) {
            $this->state->set($item->data['exception_mailer_exclude_id'] . '_last_sent', $item->data['timestamp']);
          }
        }
        catch (SuspendQueueException $e) {
          $queue->releaseItem($item);
          break;
        }
        catch (\Exception $e) {
          // Avoid infinite loops.
          $variables = Error::decodeException($e);
          $message = strtr(Error::DEFAULT_ERROR_MESSAGE, $variables);

          // @todo Find better way.
          if ($message !== $item->data['message']) {
            $this->log(RfcLogLevel::ERROR, Error::DEFAULT_ERROR_MESSAGE, $variables);
          }
        }
      }
    }
  }

  /**
   * Returns all the email addresses set in config form.
   */
  protected function getConfigFormEmailAdress(): array {
    $config = $this->configFactory->get('exception_mailer.settings');
    $email_addresses = explode(",", trim($config->get('emails')));
    if ($roles = $config->get('roles')) {
      foreach (UserRepository::getUserEmails(array_keys($roles)) as $email) {
        $email_addresses[] = $email;
      }
    }
    // Remove duplicate emails.
    return array_unique($email_addresses);
  }

  /**
   * Returns the default context for log messages.
   *
   * @return array
   *   The default context.
   */
  protected function getDefaultContext(): array {
    return [
      'timestamp' => time(),
      'uid' => '',
      'channel' => '',
      'link' => '',
      'request_uri' => '',
      'referer' => '',
      'ip' => '',
    ];
  }

}
