<?php

namespace Drupal\exception_mailer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\exception_mailer\ExceptionMailerExcludeInterface;
use Drupal\exception_mailer\Utility\UserRepository;

/**
 * Defines an Exception Mailer Exclude entities configuration entity.
 *
 * @ConfigEntityType(
 *   id = "exception_mailer_exclude",
 *   label = @Translation("Exception Mailer Exclude"),
 *   label_collection = @Translation("Excludes"),
 *   label_singular = @Translation("exclude"),
 *   label_plural = @Translation("excludes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count exclude",
 *     plural = "@count excludes",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\exception_mailer\Form\ExceptionMailerExcludeForm",
 *       "edit" = "Drupal\exception_mailer\Form\ExceptionMailerExcludeForm",
 *       "delete" = "Drupal\exception_mailer\Form\ExceptionMailerExcludeDeleteForm",
 *     },
 *     "list_builder" = "Drupal\exception_mailer\Controller\ExceptionMailerExcludeListBuilder",
 *   },
 *   admin_permission = "administer exception_mailer excludes",
 *   config_prefix = "exception",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "type",
 *     "exception",
 *     "error_type",
 *     "error_severity",
 *     "message",
 *     "hostname",
 *     "send_email",
 *     "emails",
 *     "roles",
 *     "email_body",
 *     "send_interval"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/exception-mailer/excludes/add",
 *     "edit-form" = "/admin/config/exception-mailer/excludes/{exception_mailer_exclude}",
 *     "delete-form" = "/admin/config/exception-mailer/excludes/{exception_mailer_exclude}/delete",
 *     "collection" = "/admin/config/exception-mailer/excludes",
 *   }
 * )
 */
class ExceptionMailerExclude extends ConfigEntityBase implements ExceptionMailerExcludeInterface {

  /**
   * The exclude label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the exclude.
   *
   * @var string
   */
  protected $description;

  /**
   * Error/Exception.
   *
   * @var string
   */
  protected $type;

  /**
   * Exception text.
   *
   * @var string
   */
  protected $exception;

  /**
   * Error type.
   *
   * @var string
   */
  protected $error_type;

  /**
   * Array of error severity.
   *
   * @var int[]
   */
  protected $error_severity;

  /**
   * Error/Exception message.
   *
   * @var string
   */
  protected $message;

  /**
   * Hostname/IP.
   *
   * @var string
   */
  protected $hostname;

  /**
   * Send interval in minutes (from last appear), stored as state.
   *
   * E.g.: 0 = immediately, 60 = hourly, 1440 = daily.
   *
   * @var int
   */
  protected $send_interval;

  /**
   * Send emails or not.
   *
   * @var bool
   */
  protected $send_email;

  /**
   * Array of email addresses to be sent.
   *
   * @var string[]
   */
  protected $emails;

  /**
   * User roles to be sent.
   *
   * @var string[]
   */
  protected $roles;

  /**
   * Text to be send.
   *
   * @var string
   */
  protected $email_body;

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): ?string {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getException(): ?string {
    return $this->exception;
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorType(): ?string {
    return $this->error_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorSeverity(): ?array {
    return $this->error_severity;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage(): ?string {
    return $this->message;
  }

  /**
   * {@inheritdoc}
   */
  public function getHostname(): ?string {
    return $this->hostname;
  }

  /**
   * {@inheritdoc}
   */
  public function getSendEmail(): ?bool {
    return $this->send_email;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmails(): array {
    return $this->emails ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function setEmails(array $emails): void {
    $this->emails = $emails;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailBody(): ?string {
    return $this->email_body;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles(): ?array {
    return $this->roles;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailAddresses(): array {
    if (!$this->getSendEmail()) {
      return [];
    }

    if ($this->getSendInterval() && ($this->getLastSent() + ($this->getSendInterval() * 60)) >= \Drupal::time()->getRequestTime()) {
      return [];
    }

    $email_addresses = $this->getEmails();

    if ($roles = $this->getRoles()) {
      foreach (UserRepository::getUserEmails(array_keys($roles)) as $email) {
        $email_addresses[] = $email;
      }
    }

    // Remove duplicate emails.
    return array_unique($email_addresses);
  }

  /**
   * {@inheritdoc}
   */
  public function getSendInterval(): ?int {
    return (int) $this->send_interval;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastSent(): int {
    return (int) \Drupal::state()->get($this->id() . '_last_sent');
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): bool {
    return $this->status;
  }

}
