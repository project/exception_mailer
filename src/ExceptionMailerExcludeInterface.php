<?php

namespace Drupal\exception_mailer;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Exception Mailer Exclude entities entity.
 *
 * @ingroup exception_mailer
 */
interface ExceptionMailerExcludeInterface extends ConfigEntityInterface {

  /**
   * Returns the exclude name.
   *
   * @return string
   *   The exclude name.
   */
  public function getName(): string;

  /**
   * Returns the exclude description.
   *
   * @return string|null
   *   The exclude description.
   */
  public function getDescription(): ?string;

  /**
   * Returns the exclude type.
   *
   * @return string|null
   *   The exclude type: error/exception.
   */
  public function getType(): ?string;

  /**
   * Returns the exception contains string.
   *
   * @return string|null
   *   The exception.
   */
  public function getException(): ?string;

  /**
   * Returns the error type.
   *
   * @return string|null
   *   The error type, e.g. php.
   */
  public function getErrorType(): ?string;

  /**
   * Returns array with error severity codes.
   *
   * @return int[]|null
   *   The error severity code: notice/warning/etc.
   */
  public function getErrorSeverity(): ?array;

  /**
   * Returns the exception/error message contains string.
   *
   * @return string|null
   *   The exception/error message.
   */
  public function getMessage(): ?string;

  /**
   * Returns the hostname contains string.
   *
   * @return string|null
   *   The hostname.
   */
  public function getHostname(): ?string;

  /**
   * Returns the send_email property.
   *
   * @return bool|null
   *   True if email should be sent.
   */
  public function getSendEmail(): ?bool;

  /**
   * Returns the email addresses or empty array.
   *
   * @return string[]
   *   Email addresses.
   */
  public function getEmails(): array;

  /**
   * Set email addresses.
   *
   * @param string[] $emails
   *   Email addresses.
   */
  public function setEmails(array $emails): void;

  /**
   * Returns the email body to be send.
   *
   * @return string|null
   *   Text of the email.
   */
  public function getEmailBody(): ?string;

  /**
   * Returns the array of roles.
   *
   * @return string[]|null
   *   Roles.
   */
  public function getRoles(): ?array;

  /**
   * Check data and return excludes.
   *
   * @return string[]
   *   Array of email addresses.
   */
  public function getEmailAddresses(): array;

  /**
   * Returns the send interval in minutes.
   *
   * @return int|null
   *   The send interval.
   */
  public function getSendInterval(): ?int;

  /**
   * Returns the timestamp of the last sent.
   *
   * @return int
   *   Timestamp.
   */
  public function getLastSent(): int;

  /**
   * Returns the status.
   *
   * @return bool
   *   True on active.
   */
  public function getStatus(): bool;

}
