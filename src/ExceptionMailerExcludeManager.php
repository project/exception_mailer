<?php

namespace Drupal\exception_mailer;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class ExceptionMailerExcludeManager.
 *
 * Provides a manager for Exception Mailer Exclude entities.
 */
class ExceptionMailerExcludeManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityTypeBundleInfo.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Check data and return excludes.
   *
   * @param mixed[] $data
   *   Error data.
   * @param string $type
   *   Type: 'error' | 'exception'.
   *
   * @return ExceptionMailerExcludeInterface[]
   *   Array of excludes with exclude data.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function getExcludes(array $data, string $type): array {
    if (!in_array($type, ['exception', 'error'])) {
      return [];
    }

    $excludes = $this->entityTypeManager->getStorage('exception_mailer_exclude')
      ->loadByProperties([
        'type' => $type,
        'status' => 1,
      ]);

    $this->checkExcludesForData($excludes, $data, $type);

    return $excludes;
  }

  /**
   * Check data and return excludes.
   *
   * @param ExceptionMailerExcludeInterface[] $excludes
   *   Array of excludes.
   * @param mixed[] $data
   *   Error/Exception data.
   * @param string $type
   *   Type: 'error' | 'exception'.
   */
  public function checkExcludesForData(array &$excludes, array $data, string $type): void {
    /** @var \Drupal\exception_mailer\ExceptionMailerExcludeInterface  $exclude */
    foreach ($excludes as $key => $exclude) {
      if ($type === 'error') {
        if ($exclude->getErrorType() && $exclude->getErrorType() != $data['type']) {
          unset($excludes[$key]);
        }
        if ($exclude->getErrorSeverity() && !in_array($data['severity_level'], array_keys($exclude->getErrorSeverity()))) {
          unset($excludes[$key]);
        }
      }

      if ($type === 'exception') {
        if ($exclude->getException() && !strstr($data['exception'], $exclude->getException())) {
          unset($excludes[$key]);
        }
      }

      $message = $data['message'] instanceof TranslatableMarkup ? Html::decodeEntities($data['message']->render()) : $data['message'];

      if ($exclude->getMessage() && $message && !strstr($message, $exclude->getMessage())) {
        unset($excludes[$key]);
      }
      if ($exclude->getHostname() && !strstr($data['hostname'], $exclude->getHostname())) {
        unset($excludes[$key]);
      }
    }
  }

}
