<?php

namespace Drupal\exception_mailer\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Exception Mailer Exclude entities.
 *
 * @ingroup exception_mailer
 */
class ExceptionMailerExcludeListBuilder extends ConfigEntityListBuilder {

  /**
   * Builds the header row for the entity listing.
   *
   * @return string[]
   *   A render array structure of header strings.
   *
   * @see \Drupal\Core\Entity\EntityListController::render()
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Name');
    $header['description'] = $this->t('Description');
    $header['type'] = $this->t('Type');
    $header['send_email'] = $this->t('Send email');
    $header['send_interval'] = $this->t('Send interval');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * Builds a row for an entity in the entity listing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to build the row.
   *
   * @return mixed[]
   *   A render array of the table row for displaying the entity.
   *
   * @see \Drupal\Core\Entity\EntityListController::render()
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\exception_mailer\ExceptionMailerExcludeInterface  $entity */
    $row['label'] = $entity->label();
    $row['description'] = $entity->getDescription();
    $row['type'] = $entity->getType();
    $row['send_email'] = $entity->getSendEmail() ? $this->t('Yes') : $this->t('No');
    $row['send_interval'] = $entity->getSendEmail() ? $entity->getSendInterval() : '-';
    $row['status'] = $entity->getStatus() ? $this->t('Enabled') : $this->t('Disabled');

    return $row + parent::buildRow($entity);
  }

}
