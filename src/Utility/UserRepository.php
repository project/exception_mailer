<?php

namespace Drupal\exception_mailer\Utility;

use Drupal\user\Entity\User;

/**
 * Helper class to grab user data.
 */
class UserRepository {

  /**
   * Get email addresses for a targeted user roles.
   *
   * @param string[] $roles
   *   Array of role ids of the targeted user role.
   *
   * @return string[]
   *   Returns an array of email addresses.
   */
  public static function getUserEmails(array $roles): array {
    if (empty($roles)) {
      return [];
    }

    $user_emails = [];
    $ids = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('status', 1)
      ->condition('roles', $roles, 'IN')
      ->execute();
    $users = User::loadMultiple($ids);
    foreach ($users as $user) {
      array_push($user_emails, $user->getEmail());
    }
    return $user_emails;
  }

}
