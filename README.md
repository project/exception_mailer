CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Error & Exception Mailer module sends an email when error/exception occurs.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/exception_mailer

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/exception_mailer


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Configuration » Development » Error & Exception mailer config:
 * Set on which Error level type (critical, error, warning, etc) should be email
   sent
 * Customize which role(s) (users assigned to role(s)) or email addresses
   should get email
 * Email sending limits - flood control
   * Set maximum number of suppressed consecutive similar email alerts
   * Set similarity level
 * Excludes - add exclude definitions to don't send email in specific cases


MAINTAINERS
-----------

 * Tom Bennett (tommbee) - https://www.drupal.org/u/tommbee
 * Ilma Kosa (kosa-ilma) - https://www.drupal.org/u/kosa-ilma
 * Tamás Szanyi (szato) - https://www.drupal.org/u/szato

Supporting organization:

 * Brainsum - https://www.drupal.org/brainsum
